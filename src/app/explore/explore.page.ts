import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {IonContent, IonSlides, NavController} from '@ionic/angular';
import {horizontalSlideAnimation} from "../provider/tab-animations";
import {TabService} from "../provider/tab-service";

@Component({
  selector: 'app-explore',
  templateUrl: './explore.page.html',
  styleUrls: ['./explore.page.scss'],
  animations: [
    horizontalSlideAnimation
  ]
})
export class ExplorePage implements OnInit {

  selectedContent = 0;
  items = Array(4);

  @ViewChild(IonSlides, {static : true}) slider : IonSlides;
  @ViewChild(IonContent, {static : true, read: ElementRef}) content : ElementRef;
  constructor(
      private tabs: TabService,
      private navCtrl: NavController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.initColumn();
  }

  initColumn() {
    const content = this.content.nativeElement;
    const items = content.querySelectorAll('.following-item');

    // setting item height
    items.forEach((el) => {
      (<HTMLElement> el).style.setProperty('height', (el.clientWidth + 'px'));
    });
  }

  segmentChanged(e) {
    const selectedIndex = e.detail.value;
    this.slider.slideTo(selectedIndex);
  }

  sliderChanged(e) {
    this.slider.getActiveIndex().then((v) => {
      this.selectedContent = v;
    });
  }

  gotoPage(url) {
    this.tabs.goto(4);
    this.navCtrl.navigateForward([url]);
  }
}
