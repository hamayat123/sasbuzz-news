import { Component } from '@angular/core';
import {TabService} from '../provider/tab-service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private tabs: TabService) {}

  async navGoto(no: number) {
    this.tabs.goto(no);
  }
}
