import {Component, OnInit, ViewChild} from '@angular/core';
import {IonSlides} from '@ionic/angular';
import {horizontalSlideAnimation} from '../provider/tab-animations';
import {TabService} from '../provider/tab-service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
  animations: [
    horizontalSlideAnimation
  ]
})
export class NotificationPage implements OnInit {

  selectedContent = 0;

  @ViewChild(IonSlides, {static: true}) slider: IonSlides;
  constructor(private tabs: TabService) { }

  ngOnInit() {
  }

  segmentChanged(e) {
    const selectedIndex = e.detail.value;
    this.slider.slideTo(selectedIndex);
    document.getElementsByTagName('ion-segment-button')[selectedIndex].scroll({left: 0});
  }

  sliderChanged(e) {
    this.slider.getActiveIndex().then((v) => {
      this.selectedContent = v;
    });
  }
}
