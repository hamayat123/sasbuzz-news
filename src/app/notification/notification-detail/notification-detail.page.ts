import {Component, NgZone, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.page.html',
  styleUrls: ['./notification-detail.page.scss'],
})
export class NotificationDetailPage implements OnInit {

  constructor(
      private navCtrl: NavController,
      private ngZone: NgZone) { }

  ngOnInit() {
  }

  back() {
    this.ngZone.run(() => {
      this.navCtrl.back();
    });
  }
}
