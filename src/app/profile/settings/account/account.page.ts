import {Component, NgZone, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  constructor(
      private navCtrl: NavController,
      private ngZone: NgZone) { }

  ngOnInit() {
  }
  back() {
    this.ngZone.run(() => {
      this.navCtrl.back();
    });
  }

}
