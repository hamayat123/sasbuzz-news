import {Component, NgZone, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-password',
  templateUrl: './password.page.html',
  styleUrls: ['./password.page.scss'],
})
export class PasswordPage implements OnInit {

  constructor(
      private navCtrl: NavController,
      private ngZone: NgZone) { }

  ngOnInit() {
  }
  back() {
    this.ngZone.run(() => {
      this.navCtrl.back();
    });
  }
}
