import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {IonContent, IonSlides, NavController} from '@ionic/angular';
import {TabService} from '../../provider/tab-service';

@Component({
  selector: 'app-friend-profile',
  templateUrl: './friend-profile.page.html',
  styleUrls: ['./friend-profile.page.scss'],
})
export class FriendProfilePage implements OnInit {

  selectedContent = 0;

  @ViewChild(IonSlides, {static : true}) slider: IonSlides;
  @ViewChild(IonContent, {static : true, read: ElementRef}) content: ElementRef;

  constructor(
      private tabs: TabService,
      private navCtrl: NavController,
      private ngZone: NgZone) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.initColumn();
  }

  initColumn() {
    const content = this.content.nativeElement;
    const items = content.querySelectorAll('.following-item');

    // setting item height
    items.forEach((el) => {
      (<HTMLElement> el).style.setProperty('height', (el.clientWidth + 'px'));
    });
  }

  segmentChanged(e) {
    const selectedIndex = e.detail.value;
    this.slider.slideTo(selectedIndex);
  }

  sliderChanged(e) {
    this.slider.getActiveIndex().then((v) => {
      this.selectedContent = v;
    });
  }

  gotoPage(url) {
    this.tabs.goto(6);
    this.navCtrl.navigateForward([url]);
  }
  back() {
    this.ngZone.run(() => {
      this.navCtrl.back();
    });
  }
}
