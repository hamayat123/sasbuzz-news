import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FindFriendPage } from './find-friend.page';

const routes: Routes = [
  {
    path: '',
    component: FindFriendPage
  },
  {
    path: 'friend-profile',
    loadChildren: () => import('./friend-profile/friend-profile.module').then( m => m.FriendProfilePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FindFriendPageRoutingModule {}
