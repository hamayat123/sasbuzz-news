import {Component, NgZone, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
  selector: 'app-find-friend',
  templateUrl: './find-friend.page.html',
  styleUrls: ['./find-friend.page.scss'],
})
export class FindFriendPage implements OnInit {

  currentPage = '';
  constructor(
      private navCtrl: NavController,
      private ngZone: NgZone) { }

  ngOnInit() {
  }

  back() {
    this.ngZone.run(() => {
      this.navCtrl.back();
    });
  }
}
