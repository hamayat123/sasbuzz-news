import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

const duration = '100ms';
const duration200 = '200ms';

export const fade =
    trigger('fade', [
      state('entering', style({
        opacity: 1,
      })),
      state('leaving', style({
        opacity: 0,
      })),
      transition('entering => leaving', [
        animate(duration + ' ease', style({
          opacity: 0,
        }))
      ]),
      transition('leaving => entering', [
        animate(duration + ' ease', style({
          opacity: 1,
        }))
      ]),
    ]);

export const horizontalSlideAnimation =
    trigger('vSliding', [
      state('below', style({
        transform: 'translateX(100%)'
      })),
      state('middle', style({
        transform: 'translateX(0)',
      })),
      state('above', style({
        transform: 'translateX(-100%)',
      })),
      state('-', style({
        transform: 'translateX(0)',
      })),

      transition('* => -', [
        animate(duration + ' ease-in-out', style({
          transform: 'translateX(0)',
        }))
      ]),
      transition('below => middle', [
        animate(duration + ' ease-in-out', style({
          transform: 'translateX(0)',
        }))
      ]),
      transition('middle => above', [
        animate(duration + ' ease-in-out', style({
          transform: 'translateX(-100%)',
        }))
      ]),
      transition('above => middle', [
        animate(duration + ' ease-in-out', style({
          transform: 'translateX(0)',
        }))
      ]),
      transition('middle => below', [
        animate(duration + ' ease-in-out', style({
          transform: 'translateX(100%)',
        }))
      ]),
    ]);

export const verticalSlideAnimation =
    trigger('vSliding', [
      state('below', style({
        transform: 'translateY(100%)'
      })),
      state('middle', style({
        transform: 'translateY(0)',
      })),
      state('above', style({
        transform: 'translateY(-100%)',
      })),
      state('-', style({
        transform: 'translateY(0)',
      })),

      transition('* => -', [
        animate(duration200 + ' ease-in-out', style({
          transform: 'translateY(0)',
        }))
      ]),
      transition('below => middle', [
        animate(duration200 + ' ease-in-out', style({
          transform: 'translateY(0)',
        }))
      ]),
      transition('middle => above', [
        animate(duration200 + ' ease-in-out', style({
          transform: 'translateY(-100%)',
        }))
      ]),
      transition('above => middle', [
        animate(duration200 + ' ease-in-out', style({
          transform: 'translateY(0)',
        }))
      ]),
      transition('middle => below', [
        animate(duration200 + ' ease-in-out', style({
          transform: 'translateY(100%)',
        }))
      ]),
    ]);
