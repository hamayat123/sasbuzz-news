import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {IonContent, IonSlides, NavController} from '@ionic/angular';
import {horizontalSlideAnimation} from "../provider/tab-animations";
import {TabService} from "../provider/tab-service";
import Hammer from 'hammerjs';
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  animations: [
    horizontalSlideAnimation
  ]
})
export class HomePage implements OnInit {

  selectedContent = 0;
  items = Array(6);
  elRef: HTMLElement;

  @ViewChild(IonSlides, {static : true}) slider: IonSlides;
  @ViewChild('header', {static : true, read: ElementRef}) header: ElementRef;
  @ViewChild('content', {static : true, read: ElementRef}) content: ElementRef;
  constructor(
      private navCtrl: NavController,
      private elementRef: ElementRef,
      private tabs: TabService) {

    this.elRef = elementRef.nativeElement;

  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.initScroll();
    this.initImageSize();
  }

  segmentChanged(e) {
    const selectedIndex = e.detail.value;
    this.slider.slideTo(selectedIndex);
  }

  sliderChanged(e) {
    this.slider.getActiveIndex().then((v) => {
      this.selectedContent = v;
    });
  }

  initImageSize() {
    const header = this.header.nativeElement.clientHeight;
    const tab = document.querySelector('ion-tab-bar').clientHeight;
    const content = this.content.nativeElement;
    const mainBanner = content.querySelectorAll('.main-banner');
    const secondaryBanner = content.querySelectorAll('.following-item');

    // setting main banner height
    mainBanner.forEach((el) => {
      (<HTMLElement> el).style.setProperty('height', ((content.clientHeight - header - tab) * 0.65) + 'px');
    });
    // setting secondary banner height
    secondaryBanner.forEach((el) => {
      (<HTMLElement> el).style.setProperty('height', ((content.clientHeight - header - tab) * 0.4) + 'px');
    });
    // setting padding top di content.
    content.style.setProperty('--padding-top', header + 'px');
  }

  initScroll() {
    const element = this.content.nativeElement;
    const mc = new Hammer(element);
    const that = this;
    mc.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });

    mc.on("swipeup", function () {
      that.gotoNext();
    });

    mc.on("swipedown", function () {
      that.gotoPrev();
    });
  }

  async gotoNext() {
    this.tabs.goto(2);
    this.navCtrl.navigateForward(['/tabs/home/article']);
  }

  async gotoPrev() {
  }

  gotoPage(url) {
    this.tabs.goto(2);
    this.navCtrl.navigateForward([url]);
  }
}
