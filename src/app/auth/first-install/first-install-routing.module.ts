import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirstInstallPage } from './first-install.page';

const routes: Routes = [
  {
    path: '',
    component: FirstInstallPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FirstInstallPageRoutingModule {}
