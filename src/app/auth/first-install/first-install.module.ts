import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FirstInstallPageRoutingModule } from './first-install-routing.module';

import { FirstInstallPage } from './first-install.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FirstInstallPageRoutingModule
  ],
  declarations: [FirstInstallPage]
})
export class FirstInstallPageModule {}
