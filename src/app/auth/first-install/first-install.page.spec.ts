import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FirstInstallPage } from './first-install.page';

describe('FirstInstallPage', () => {
  let component: FirstInstallPage;
  let fixture: ComponentFixture<FirstInstallPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstInstallPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FirstInstallPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
