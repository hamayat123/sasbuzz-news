import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {verticalSlideAnimation} from '../provider/tab-animations';
import {NavController} from '@ionic/angular';
import Hammer from 'hammerjs';
import {TabService} from "../provider/tab-service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-article',
  templateUrl: './article.page.html',
  styleUrls: ['./article.page.scss'],
  animations: [
    verticalSlideAnimation
  ]
})
export class ArticlePage implements OnInit {

  id = 0;
  currentTab = 0;
  page = 'home';
  elRef: HTMLElement

  @ViewChild('header', {static : true, read: ElementRef}) header: ElementRef;
  @ViewChild('content', {static : true, read: ElementRef}) content: ElementRef;
  constructor(
      private navCtrl: NavController,
      private activatedRoute: ActivatedRoute,
      private elementRef: ElementRef,
      private tabs: TabService,
      private ngZone: NgZone,
      private router: Router) {
    this.elRef = elementRef.nativeElement;
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((param) => {
      if(param.id){
        this.id = param.id;
      }
    });
    this.page = this.router.url.split('/')[2];
    switch(this.page){
      case 'home':
        this.currentTab = 2;
        break;
      case 'following':
        this.currentTab = 3;
        break;
      case 'explore':
        this.currentTab = 4;
        break;
      default:
        this.currentTab = 6;
        break;
    }
  }

  ionViewWillEnter() {
    this.initScroll();
    this.initImageSize();
    this.tabs.beActive();
  }

  ionViewWillLeave() {
    this.tabs.beActive();
  }

  initImageSize() {
    const header = this.header.nativeElement.clientHeight;
    const tab = document.querySelector('ion-tab-bar').clientHeight;
    const content = this.content.nativeElement;

    // setting padding top di content.
    content.style.setProperty('--padding-top', header + 'px');
  }

  initScroll() {
    const element = this.content.nativeElement;
    const mc = new Hammer(element);
    const that = this;
    mc.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });

    mc.on("swipeup", function () {
      that.gotoNext();
    });

    mc.on("swipedown", function () {
      that.gotoPrev();
    });
  }

  async gotoNext() {
    let id = Math.floor(Math.random()*10);
    this.tabs.goto(this.currentTab+1);
    this.router.navigate(['/tabs/'+this.page+'/article'], {queryParams: {id: id}});
  }

  async gotoPrev() {
    this.tabs.goto(this.currentTab-1);
    this.navCtrl.back();
  }

  gotoDetail(id) {
    this.tabs.goto(this.currentTab+1);
    this.navCtrl.navigateForward(['/tabs/'+this.page+'/article/article-detail'], {queryParams: {id: id}});
  }

  back() {
    this.ngZone.run(() => {
      switch(this.page){
        case 'home':
          this.tabs.goto(1);
          break;
        case 'following':
          this.tabs.goto(2);
          break;
        case 'explore':
          this.tabs.goto(3);
          break;
        default:
          this.tabs.goto(5);
          break;
      }
      this.navCtrl.navigateBack(['/tabs/'+this.page]);
    });
  }
}
