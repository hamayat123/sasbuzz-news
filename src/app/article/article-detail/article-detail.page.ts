import {Component, NgZone, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.page.html',
  styleUrls: ['./article-detail.page.scss'],
})
export class ArticleDetailPage implements OnInit {

  currentPage = '';
  constructor(
      private navCtrl: NavController,
      private ngZone: NgZone) { }

  ngOnInit() {
  }

  back() {
    this.ngZone.run(() => {
      this.navCtrl.back();
    });
  }
}
