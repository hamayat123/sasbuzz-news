import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowingPage } from './following.page';

const routes: Routes = [
  {
    path: '',
    component: FollowingPage
  },
  {
    path: 'article',
    loadChildren: () => import('../article/article.module').then(m => m.ArticlePageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowingPageRoutingModule {}
