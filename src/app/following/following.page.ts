import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {horizontalSlideAnimation} from "../provider/tab-animations";
import {TabService} from "../provider/tab-service";
import {IonContent, NavController} from "@ionic/angular";

@Component({
  selector: 'app-following',
  templateUrl: './following.page.html',
  styleUrls: ['./following.page.scss'],
  animations: [
    horizontalSlideAnimation
  ]
})
export class FollowingPage implements OnInit {

  @ViewChild(IonContent, {static : true, read: ElementRef}) content : ElementRef;

  constructor(
      private tabs: TabService,
      private navCtrl: NavController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.initColumn();
  }

  initColumn() {
    const content = this.content.nativeElement;
    const items = content.querySelectorAll('.following-item');

    // setting item height
    items.forEach((el) => {
      (<HTMLElement> el).style.setProperty('height', (el.clientWidth + 'px'));
    });
  }

  gotoPage(url) {
    this.tabs.goto(3);
    this.navCtrl.navigateForward([url]);
  }
}
